package com.system.account.repositories.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Joseph on 2021/4/17.
 */
@Entity
@Table(name = "user_detail")
public class UserDetailsEntity {
    @Id
    private int id;

    @Column
    private String name;

    @Column
    private char gender;

    @Column(name = "LOGIN_STATUS")
    private String loginStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LOGIN_DATE")
    private Date loginDate;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private UserEntity userEntity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}