package com.system.account.repositories;

import com.system.account.repositories.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joseph on 2021/4/17.
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    List<UserEntity> findAll();
    Optional<UserEntity> findByAccount(String account);
    boolean existsByAccount(String account);
    void deleteByAccount(String account);
}