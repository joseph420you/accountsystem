package com.system.account.constants;

/**
 * Created by Joseph on 2021/4/20.
 */
public enum Gender {
    M,
    F
}