package com.system.account.constants;

/**
 * Created by Joseph on 2021/4/20.
 */
public enum LoginStatus {
    ONLINE,
    OFFLINE;

    public boolean isOnline(){
        return this.equals(ONLINE);
    }

    public boolean isOffOnline(){
        return this.equals(OFFLINE);
    }
}