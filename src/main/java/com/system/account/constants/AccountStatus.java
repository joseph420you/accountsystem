package com.system.account.constants;

/**
 * Created by Joseph on 2021/4/19.
 */
public enum AccountStatus {
    LOCKED,
    UNLOCKED;

    public boolean isLocked(){
        return this.equals(LOCKED);
    }

    public boolean isUnLocked(){
        return this.equals(UNLOCKED);
    }
}