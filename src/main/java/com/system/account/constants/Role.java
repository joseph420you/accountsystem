package com.system.account.constants;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by Joseph on 2021/4/19.
 */
public enum Role implements GrantedAuthority {
    USER,
    ADMIN;

    @Override
    public String getAuthority() {
        return this.name();
    }
}