package com.system.account.services.dto;

import com.system.account.constants.Gender;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Created by Joseph on 2021/4/24.
 */
public class UserRegisterInfoDto {
    @NotBlank
    private final String account;

    @NotBlank
    private final String password;

    @NotBlank
    private final String username;

    @NotNull
    private final Gender gender;

    public UserRegisterInfoDto(String account, String password, String username, Gender gender) {
        this.account = account;
        this.password = password;
        this.username = username;
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getAccount() {
        return account;
    }

    public Gender getGender() {
        return gender;
    }
}