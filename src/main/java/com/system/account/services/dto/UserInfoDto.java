package com.system.account.services.dto;

import com.system.account.constants.Gender;
import com.system.account.constants.LoginStatus;
import com.system.account.constants.Role;
import com.system.account.logics.bo.UserDetailsBo;

import java.util.Date;

/**
 * Created by Joseph on 2021/5/15.
 */
public class UserInfoDto {
    private final UserDetailsBo userDetails;

    public UserInfoDto(UserDetailsBo userDetails) {
        this.userDetails = userDetails;
    }

    public long getId() {
        return userDetails.getId();
    }

    public String getAccount() {
        return userDetails.getAccount();
    }

    public String getUsername() {
        return userDetails.getUsername();
    }

    public Gender getGender() {
        return userDetails.getGender();
    }

    public Date getLoginDate() {
        return userDetails.getLoginDate();
    }

    public LoginStatus getLoginStatus() {
        return userDetails.getLoginStatus();
    }

    public Role getRole() {
        return userDetails.getRole();
    }

    public boolean isAccountNonLocked() {
        return userDetails.isAccountNonLocked();
    }
}