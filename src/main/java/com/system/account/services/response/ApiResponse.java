package com.system.account.services.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.http.HttpStatus;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

/**
 * Created by Joseph on 2021/6/5.
 */
@JsonInclude(NON_EMPTY)
@JsonPropertyOrder({"code", "httpStatus", "data", "message"})
public class ApiResponse {
    private final int code;
    private final HttpStatus httpStatus;
    private final Object data;
    private final String message;

    public static ResponseBuilder getBuilder() {
        return new ResponseBuilder();
    }

    public static class ResponseBuilder {
        private static final int UNDEFINED = -1;
        private int code = UNDEFINED;
        private HttpStatus httpStatus;
        private Object data;
        private String message;

        private ResponseBuilder() {
            this.httpStatus = HttpStatus.OK;
        }

        public ResponseBuilder setCode(int code) {
            this.code = code;
            return this;
        }

        public ResponseBuilder setHttpStatus(HttpStatus httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        public ResponseBuilder setData(Object data) {
            this.data = data;
            return this;
        }

        public ResponseBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public ApiResponse build() {
            if (code == UNDEFINED) {
                return new ApiResponse(httpStatus, data, message);
            }
            return new ApiResponse(code, httpStatus, data, message);
        }
    }

    private ApiResponse(HttpStatus httpStatus, Object data, String message) {
        this.code = httpStatus.value();
        this.httpStatus = httpStatus;
        this.data = data;
        this.message = message;
    }

    private ApiResponse(int code, HttpStatus httpStatus, Object data, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.data = data;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public Object getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}