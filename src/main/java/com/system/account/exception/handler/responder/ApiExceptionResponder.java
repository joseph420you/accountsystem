package com.system.account.exception.handler.responder;

import com.system.account.exception.checked.ApiException;
import com.system.account.services.response.ApiResponse;
import org.springframework.http.HttpStatus;

/**
 * Created by Joseph on 2021/7/6.
 */
public final class ApiExceptionResponder extends ErrorResponder<ApiException> {
    public ApiExceptionResponder(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public ApiResponse getResponse(ApiException e) {
        return ApiResponse.getBuilder()
                .setCode(Integer.parseInt(e.getCode()))
                .setHttpStatus(getHttpStatus())
                .setMessage(e.getMessage())
                .build();
    }
}