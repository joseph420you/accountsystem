package com.system.account.exception.handler.responder;

import com.system.account.services.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joseph on 2021/7/6.
 */
public final class MethodArgumentExceptionResponder extends ErrorResponder<MethodArgumentNotValidException> {
    public MethodArgumentExceptionResponder(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public ApiResponse getResponse(MethodArgumentNotValidException e) {
        return ApiResponse.getBuilder()
                .setHttpStatus(getHttpStatus())
                .setData(getErrorMessage(e))
                .build();
    }

    private List<String> getErrorMessage(MethodArgumentNotValidException e) {
        List<String> errorMsg = new ArrayList<>();

        for (final FieldError error : e.getBindingResult().getFieldErrors()) {
            errorMsg.add(error.getDefaultMessage());
        }

        return errorMsg;
    }
}