package com.system.account.exception.handler;

import com.system.account.exception.checked.ApiException;
import com.system.account.exception.checked.account.IllegalAccountStateException;
import com.system.account.exception.handler.responder.ApiExceptionResponder;
import com.system.account.exception.handler.responder.MethodArgumentExceptionResponder;
import com.system.account.services.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by Joseph on 2021/7/6.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@RestControllerAdvice
public class BadRequestHandler {
    private final ApiExceptionResponder apiResponder;
    private final MethodArgumentExceptionResponder argumentResponder;

    public BadRequestHandler() {
        this.apiResponder = new ApiExceptionResponder(HttpStatus.BAD_REQUEST);
        this.argumentResponder = new MethodArgumentExceptionResponder(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            IllegalAccountStateException.class
    })
    public ApiResponse handleException(ApiException e) {
        return apiResponder.getResponse(e);
    }

    @ExceptionHandler({
            MethodArgumentNotValidException.class
    })
    public ApiResponse handleException(MethodArgumentNotValidException e) {
        return argumentResponder.getResponse(e);
    }
}