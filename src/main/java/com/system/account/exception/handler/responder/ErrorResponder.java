package com.system.account.exception.handler.responder;

import com.system.account.services.response.ApiResponse;
import org.springframework.http.HttpStatus;

/**
 * Created by Joseph on 2021/7/6.
 */
abstract class ErrorResponder<T extends Exception> {
    private final HttpStatus httpStatus;

    ErrorResponder(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public abstract ApiResponse getResponse(T e);
}