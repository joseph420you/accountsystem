package com.system.account.exception.checked.account;

import com.system.account.logics.bo.UserDetailsBo;

/**
 * Created by Joseph on 2021/7/6.
 */
public final class IllegalAccountStateException extends AccountException {

    public IllegalAccountStateException(String errorMsg) {
        super(errorMsg);
    }

    public IllegalAccountStateException(UserDetailsBo userDetailsBo) {
        this(userDetailsBo.getAccount() + " is in login status: " + userDetailsBo.getLoginStatus());
    }

    @Override
    protected String getSubCode() {
        return "01";
    }
}