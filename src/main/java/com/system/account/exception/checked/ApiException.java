package com.system.account.exception.checked;

/**
 * Created by Joseph on 2021/7/6.
 */
public abstract class ApiException extends Exception {
    protected ApiException(String errorMsg) {
        super(errorMsg);
    }

    protected ApiException(String errorMsg, Throwable e) {
        super(errorMsg, e);
    }

    public String getCode() {
        return getGroupCode() + getSubCode();
    }

    protected abstract String getGroupCode();

    protected abstract String getSubCode();
}