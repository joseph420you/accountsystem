package com.system.account.exception.checked.account;

import com.system.account.exception.checked.ApiException;

/**
 * Created by Joseph on 2021/7/6.
 */
public abstract class AccountException extends ApiException {
    public AccountException(String errorMsg) {
        super(errorMsg);
    }

    public AccountException(String errorMsg, Throwable e) {
        super(errorMsg, e);
    }

    @Override
    public String getGroupCode() {
        return "20";
    }
}