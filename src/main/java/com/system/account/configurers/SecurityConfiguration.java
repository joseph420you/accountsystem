package com.system.account.configurers;

import com.system.account.logics.system.JpaAuthenticationProvider;
import com.system.account.logics.UserDetailsManagerImp;
import com.system.account.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;

/**
 * Created by Joseph on 2021/4/22.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserRepository repo;

    @Autowired
    public SecurityConfiguration(UserRepository repo) {
        this.repo = repo;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(jpaAuthenticationProvider());
    }

    @Bean
    public JpaAuthenticationProvider jpaAuthenticationProvider() {
        return new JpaAuthenticationProvider(userDetailsService(), passwordEncoder());
    }

    @Override
    @Bean
    protected UserDetailsManagerImp userDetailsService() {
        return new UserDetailsManagerImp(repo, passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
//                .anyRequest().permitAll()
                .and()
                .formLogin()
//                    .loginProcessingUrl("/doLogin")
//                    .defaultSuccessUrl("/index")
//                    .failureUrl("/fail")
                .and()
//                .rememberMe().key("jojo")
//                .and()
                .csrf().disable();
    }

    @Bean
    HttpSessionEventPublisher httpSessionEventPublisher(){
        return new HttpSessionEventPublisher();
    }
}