package com.system.account.logics.bo;

import com.system.account.constants.AccountStatus;
import com.system.account.constants.Gender;
import com.system.account.constants.LoginStatus;
import com.system.account.constants.Role;
import com.system.account.repositories.entities.UserDetailsEntity;
import com.system.account.repositories.entities.UserEntity;
import com.system.account.services.dto.UserRegisterInfoDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Joseph on 2021/4/19.
 */
public class UserDetailsBo implements UserDetails {
    private final int id;
    private final String account;
    private final String password;
    private final AccountStatus accountStatus;
    private final List<GrantedAuthority> authority;
    private final Role role;
    private final String name;
    private final Gender gender;
    private final LoginStatus loginStatus;
    private final Date loginDate;

    public static UserDetailsBuilder getBuilder() {
        return new UserDetailsBuilder();
    }

    public static class UserDetailsBuilder {
        private int id;
        private String account = "";
        private String password = "";
        private AccountStatus accountStatus = AccountStatus.UNLOCKED;
        private String name = "";
        private Gender gender;
        private LoginStatus loginStatus = LoginStatus.OFFLINE;
        private Role role = Role.USER;
        private Date loginDate;

        private UserDetailsBuilder() {
        }

        public UserDetailsBuilder setAccount(String account) {
            this.account = account;
            return this;
        }

        public UserDetailsBuilder setId(int id) {
            this.id = id;
            return this;
        }

        public UserDetailsBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserDetailsBuilder setAccountStatus(AccountStatus accountStatus) {
            this.accountStatus = accountStatus;
            return this;
        }

        public UserDetailsBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserDetailsBuilder setGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public UserDetailsBuilder setLoginStatus(LoginStatus loginStatus) {
            this.loginStatus = loginStatus;
            return this;
        }

        public UserDetailsBuilder setRole(Role role) {
            this.role = role;
            return this;
        }

        public UserDetailsBuilder setLoginDate(Date loginDate) {
            this.loginDate = loginDate;
            return this;
        }

        public UserDetailsBo build() {
            return new UserDetailsBo(this);
        }

        public UserDetailsBo build(UserEntity entity) {
            Assert.notNull(entity, "UserEntity must not be null");
            Assert.notNull(entity.getUserDetailsEntity(), "UserDetailsEntity must not be null");

            UserDetailsEntity userDetailsEntity = entity.getUserDetailsEntity();

            setId(entity.getId());
            setAccount(entity.getAccount());
            setPassword(entity.getPassword());
            setAccountStatus(AccountStatus.valueOf(entity.getAccountStatus()));
            setName(userDetailsEntity.getName());
            setGender(Gender.valueOf(Character.toString(userDetailsEntity.getGender())));
            setLoginStatus(LoginStatus.valueOf(userDetailsEntity.getLoginStatus()));
            setRole(Role.valueOf(entity.getRole()));
            setLoginDate(userDetailsEntity.getLoginDate());

            return build();
        }

        public UserDetailsBo build(UserRegisterInfoDto dto) {
            Assert.notNull(dto, "UserRegisterInfoDto must not be null");

            setAccount(dto.getAccount());
            setPassword(dto.getPassword());
            setAccountStatus(AccountStatus.UNLOCKED);
            setName(dto.getUsername());
            setGender(dto.getGender());
            setLoginStatus(LoginStatus.OFFLINE);
            setRole(Role.USER);

            return build();
        }
    }

    private UserDetailsBo(UserDetailsBuilder builder) {
        this.id = builder.id;
        this.account = builder.account;
        this.password = builder.password;
        this.accountStatus = builder.accountStatus;
        this.role = builder.role;
        this.name = builder.name;
        this.gender = builder.gender;
        this.loginStatus = builder.loginStatus;
        this.authority = AuthorityUtils.createAuthorityList(role.toString());
        this.loginDate = builder.loginDate;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountStatus.isUnLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public int getId() {
        return id;
    }

    public String getAccount() {
        return account;
    }

    public Gender getGender() {
        return gender;
    }

    public Role getRole() {
        return role;
    }

    public LoginStatus getLoginStatus() {
        return loginStatus;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public UserEntity getEntity() {
        UserEntity userEntity = getUserEntity();
        UserDetailsEntity userDetailsEntity = getDetailEntity();
        userEntity.setUserDetailsEntity(userDetailsEntity);
        userDetailsEntity.setUserEntity(userEntity);

        return userEntity;
    }

    private UserEntity getUserEntity() {
        UserEntity entity = new UserEntity();
        entity.setAccount(account);
        entity.setPassword(password);
        entity.setAccountStatus(accountStatus.toString());
        entity.setRole(role.toString());

        return entity;
    }

    public UserDetailsEntity getDetailEntity() {
        UserDetailsEntity entity = new UserDetailsEntity();
        entity.setName(name);
        entity.setGender(gender.toString().charAt(0));
        entity.setLoginStatus(loginStatus.toString());
        return entity;
    }
}