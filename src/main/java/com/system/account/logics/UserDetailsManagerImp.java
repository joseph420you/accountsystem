package com.system.account.logics;

import com.system.account.constants.AccountStatus;
import com.system.account.constants.LoginStatus;
import com.system.account.constants.Role;
import com.system.account.exception.checked.account.IllegalAccountStateException;
import com.system.account.logics.bo.UserDetailsBo;
import com.system.account.repositories.UserRepository;
import com.system.account.repositories.entities.UserEntity;
import com.system.account.services.dto.UserRegisterInfoDto;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Joseph on 2021/4/17.
 */
public class UserDetailsManagerImp implements UserDetailsManager, UserDetailsPasswordService {
    private final UserRepository repo;
    private final PasswordEncoder passwordEncoder;

    public UserDetailsManagerImp(UserRepository repo, PasswordEncoder passwordEncoder) {
        this.repo = repo;
        this.passwordEncoder = passwordEncoder;
    }

    public void createUser(UserRegisterInfoDto dto) {
        UserDetailsBo info = UserDetailsBo.getBuilder()
                .setAccount(dto.getAccount())
                .setAccountStatus(AccountStatus.UNLOCKED)
                .setGender(dto.getGender())
                .setLoginStatus(LoginStatus.OFFLINE)
                .setRole(Role.USER)
                .setPassword(passwordEncoder.encode(dto.getPassword()))
                .setName(dto.getUsername())
                .build();

        createUser(info);
    }

    @Override
    public void createUser(UserDetails user) {
        assertUserNotExists(user);
        assertUserDetailsType(user);

        repo.save(((UserDetailsBo) user).getEntity());
    }

    private void assertUserNotExists(UserDetails user) {
        assertUserNotExists(user.getUsername());
    }

    private void assertUserNotExists(String username) {
        Assert.isTrue(!userExists(username), "user should not exist");
    }

    @Override
    public boolean userExists(String username) {
        return repo.existsByAccount(username);
    }


    private void assertUserDetailsType(UserDetails user) {
        Class<UserDetailsBo> tClass = UserDetailsBo.class;
        Assert.isInstanceOf(tClass, user, "class type error: " + tClass.getSimpleName() + " is the only legal type");
    }

    @Override
    public void updateUser(UserDetails user) {
        assertUserExists(user);
        assertUserDetailsType(user);

        UserDetailsBo info = (UserDetailsBo) user;

        repo.save(info.getEntity());
    }

    private void assertUserExists(UserDetails user) {
        assertUserExists(user.getUsername());
    }

    private void assertUserExists(String username) {
        Assert.isTrue(userExists(username), "user should exist");
    }

    @Override
    public void deleteUser(String account) {
        assertAccountNotBeEmpty(account);
        repo.deleteByAccount(account);
    }

    @Override
    public void changePassword(String credential, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null) {
            // This would indicate bad coding somewhere
            throw new AccessDeniedException(
                    "Can't change password as no Authentication object found in context " + "for current user.");
        }
        String username = currentUser.getName();

        assertUserExists(username);

        UserEntity entity = repo.findByAccount(username).get();
        String oldPassword = entity.getPassword();

        if (!credential.equals(oldPassword)) {
            throw new BadCredentialsException("Reauthenticating user " + username + " for password change request.");
        }

        entity.setPassword(newPassword);
        repo.save(entity);
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        assertUserExists(user);
        Assert.hasText(newPassword, "new password must not be empty");

        UserEntity entity = repo.findByAccount(user.getUsername()).get();
        entity.setPassword(newPassword);

        repo.save(entity);

        return UserDetailsBo.getBuilder().build(entity);
    }

    @Override
    public UserDetailsBo loadUserByUsername(String account) throws UsernameNotFoundException {
        Optional<UserEntity> dbData = repo.findByAccount(account);

        UserEntity entity = dbData.orElseThrow(() -> new UsernameNotFoundException(account));

        return UserDetailsBo.getBuilder().build(entity);
    }

    public List<UserDetailsBo> getAllUsers() {
        List<UserDetailsBo> bo = new ArrayList<>();
        List<UserEntity> allEntities = repo.findAll();

        allEntities.forEach(entity -> bo.add(UserDetailsBo.getBuilder().build(entity)));

        return bo;
    }

    /**
     * 只能刪除離線帳戶
     *
     * @param account 帳號名稱
     * @throws IllegalAccountStateException 帳號非離線狀態
     */
    public void safeDeleteUser(String account) throws IllegalAccountStateException {
        assertAccountNotBeEmpty(account);

        UserDetailsBo bo = loadUserByUsername(account);
        if (bo.getLoginStatus().isOnline()) {
            throw new IllegalAccountStateException(bo);
        }

        repo.deleteByAccount(account);
    }

    private void assertAccountNotBeEmpty(String account) {
        Assert.hasText(account, "account must not be empty");
    }

    /**
     * 鎖定帳號
     *
     * @param username 帳號名稱
     */
    public void lockUser(String username) {
        assertUserExists(username);

        UserDetailsBo bo = loadUserByUsername(username);

        if (bo.isAccountNonLocked()) {
            UserEntity entity = bo.getEntity();
            entity.setAccountStatus(AccountStatus.LOCKED.toString());
        }
    }

    /**
     * 解除鎖定帳號
     *
     * @param username 帳號名稱
     */
    public void unlockUser(String username) {
        assertUserExists(username);

        UserDetailsBo bo = loadUserByUsername(username);

        if (bo.isAccountNonLocked()) {
            return;
        }

        UserEntity entity = bo.getEntity();
        entity.setAccountStatus(AccountStatus.UNLOCKED.toString());
    }
}